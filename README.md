# Stimmzettel

Template für FSP Stimmzettel

## How-To

1. Forken und Kopieren
1. Siehe Kommentare in der `.tex`-Datei.
1. `default`-Target im Makefile anpassen

Die Daten selbst werden aus der `.csv`-Datei ausgelesen.
