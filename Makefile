
.PHONY: default
default: stimmzettel_template.pdf legislatur_2024.generic.pdf


.PHONY: clean
clean:
	$(RM) *.aux *.log
	$(RM) *.pdf

%.tex: %.json template.tex
	jinja2 --strict -o "$@" template.tex "$(subst .tex,.json,$@)"

%.generic.tex: %.generic.json generic_template.tex
	jinja2 --strict -o "$@" generic_template.tex "$(subst .tex,.json,$@)"

%.pdf: %.tex
	pdflatex $(subst .pdf,.tex,$@)
	pdflatex $(subst .pdf,.tex,$@)
	${RM} $(subst .pdf,.log,$@) $(subst .pdf,.out,$@) $(subst .pdf,.aux,$@) $(subst .pdf,.toc,$@)
